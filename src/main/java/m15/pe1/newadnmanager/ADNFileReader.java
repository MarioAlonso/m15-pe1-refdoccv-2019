/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m15.pe1.newadnmanager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author marioalonso
 */
public class ADNFileReader {
    
    /**
     * Funció que llegeix una cadena d'ADN d'un fitxer específic i la retorna
     * 
     * @param path
     * @return dnaSequence
     */
    public ArrayList readSequence (String path){
        ArrayList<String> dnaSequence = new ArrayList();
        try (FileReader reader = new FileReader(path);
                
                BufferedReader br = new BufferedReader(reader)) {

            // read line by line
            String line;
            while ((line = br.readLine()) != null) {
                dnaSequence.add(line);
            }

        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
        return dnaSequence;
    }

    

}
