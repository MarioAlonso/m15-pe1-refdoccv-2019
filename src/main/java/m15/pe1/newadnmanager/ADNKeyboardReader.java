/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m15.pe1.newadnmanager;

import java.util.Scanner;

/**
 *
 * @author marioalonso
 */
public class ADNKeyboardReader {

    /**
     * Funció que demana a l'usuari una cadena d'ADN per teclat i la retorna
     * 
     * @return ADN
     */
    public String ANDReader() {
        Scanner sc = new Scanner(System.in);
        String ADN = "";
        
        System.out.print("Introduce the ADN sequence: ");
        ADN = sc.nextLine();
        return ADN;
    }

}
