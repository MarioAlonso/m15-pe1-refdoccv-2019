package m15.pe1.newadnmanager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author marioalonso
 */
public class DnaNewExamMain {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        DnaNewExamMain myApp = new DnaNewExamMain();
        myApp.run();
    }

    private static final String TOTAL_DE_ = "Total de";

    private void run() {
        int option = 0;
        ADN_Manager cadenaADN = new ADN_Manager();
//        ADNFileReader file = new ADNFileReader();
//        ArrayList<String> dnaSequence_list = file.readSequence("src/main/java/m15/pe1/newadnmanager/dnaSequence.txt");
//        String dnaSequence = String.join("", dnaSequence_list);
//        dnaSequence = dnaSequence.toUpperCase();

        ADNKeyboardReader sequence = new ADNKeyboardReader();
        String dnaSequence = sequence.ANDReader();
        dnaSequence = dnaSequence.toUpperCase();
    

        do {
            option = printMenu();
            switch (option) {
                case 0:
                    System.out.println("Bye!");
                    break;
                case 1:
                    System.out.println("**Donar la volta "+dnaSequence+"**");
                    System.out.println("");
                    System.out.println(cadenaADN.invertADN(dnaSequence));
                    break;
                case 2:
                    System.out.println("**Trobar la base més repetida "+dnaSequence+"**");
                    System.out.println("");
                    System.out.println(cadenaADN.maxLetter(dnaSequence));
                    break;
                case 3:
                    System.out.println("**Trobar la base menys repetida "+dnaSequence+"**");
                    System.out.println("");
                    System.out.println(cadenaADN.minLetter(dnaSequence));
                    break;
                case 4:
                    System.out.println("**Fer recompte de bases "+dnaSequence+"**");
                    System.out.println("");
                     System.out.println( TOTAL_DE_ + " A: "+cadenaADN.numAdenines(dnaSequence));
                     System.out.println(TOTAL_DE_ + " C: "+cadenaADN.numCitosines(dnaSequence));
                     System.out.println(TOTAL_DE_ + " G: "+cadenaADN.numGuanines(dnaSequence));
                     System.out.println(TOTAL_DE_ + " T: "+cadenaADN.numTimines(dnaSequence));
                    break;
                case 5:
                    System.out.println("**Fer recompte del percentatge de bases " + dnaSequence + "**");
                    System.out.println("");
                    System.out.println(TOTAL_DE_ + " A: "+cadenaADN.percentageAdenines(dnaSequence) + "%");
                    System.out.println(TOTAL_DE_ + " C: "+cadenaADN.percentageCitosines(dnaSequence)+ "%");
                    System.out.println(TOTAL_DE_ + " G: "+cadenaADN.percentageGuanines(dnaSequence)+ "%");
                    System.out.println(TOTAL_DE_ + " T: "+cadenaADN.percentageTimines(dnaSequence)+ "%");
                    break;
                default:
                    System.out.println("La opció seleccionada no és válida.");
                    break;
            }
        } while (option != 0);
    }

   
    /**
     * Funció que imprimeix el menú amb totes les opcions
     * 
     * @return option 
     */
    private static int printMenu(){
        int option = 0;
        System.out.println("\n Tria una opció:");
            System.out.println("0.-Sortir");
            System.out.println("1.-Donar la volta\n"
                    + "2.- Trobar la base més repetida,\n"
                    + "3.- Trobar la base menys repetida\n"
                    + "4.- Fer recompte de bases\n"
                    + "5.- Calcular percentatje de bases\n");
            System.out.print("\nOpció: ");
            Scanner myScan = new Scanner(System.in);
            option = myScan.nextInt();

            
            return option;
    }
}
