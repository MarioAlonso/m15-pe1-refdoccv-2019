/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m15.pe1.newadnmanager;

/**
 *
 * @author marioalonso
 */
public class ADN_Manager {

    /**
     * Funcio que agafa l'atribut ADN i el retorna invertit
     *
     * @param ADN
     * @return ADN invertit.
     */
    public String invertADN(String ADN) {
        StringBuilder builder = new StringBuilder(ADN);
        return builder.reverse().toString();
    }

    /**
     * Fa recompte de totes les A's i retorna la quantitat
     *
     * @param ADN
     * @return Numero de adenines acumulades a tota la cadena
     */
    public int numAdenines(String ADN) {
        int a = 0;
        //char[] letter = this.adn.toUpperCase().toCharArray();
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'A') {
                a++;
            }
        }
        return a;
    }

    /**
     * Fa recompte de totes les G's i retorna la quantitat
     *
     * @param ADN
     * @return Numero de adenines acumulades a tota la cadena
     */
    public int numGuanines(String ADN) {
        int g = 0;
        //char[] letter = this.adn.toUpperCase().toCharArray();
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'G') {
                g++;
            }
        }
        return g;
    }

    /**
     * Fa recompte de totes les T's i retorna la quantitat
     *
     * @param ADN
     * @return Numero de adenines acumulades a tota la cadena
     */
    public int numTimines(String ADN) {
        int t = 0;
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'T') {
                t++;
            }
        }
        return t;
    }

    /**
     * Fa recompte de totes les C's i retorna la quantitat
     *
     * @param ADN
     * @return Numero de adenines acumulades a tota la cadena
     */
    public int numCitosines(String ADN) {
        int c = 0;
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'C') {
                c++;
            }
        }
        return c;
    }

    /**
     * Funcio que compara el recompte de totes les lletres
     *
     * @param ADN
     * @return La lletra que té mes recompte que la resta
     */
    public String maxLetter(String ADN) {
        int max = 0;
        String base;
        int a = numAdenines(ADN);
        int c = numCitosines(ADN);
        int g = numGuanines(ADN);
        int t = numTimines(ADN);

        if (a > c && a > g && a > t) {
            base = "A";
            max = a;
        } else if (c > a && c > g && c > t) {
            base = "C";
            max = c;
        } else if (g > a && g > c && g > t) {
            base = "G";
            max = g;
        } else {
            base = "T";
            max = t;
        }

        return base;
    }
 /**
     * Funcio que compara el recompte de totes les lletres
     *
     * @param ADN
     * @return La lletra que té menys recompte que la resta
     * 
     */
    public String minLetter(String ADN) {
        int min = 0;
        String base;
        int a = numAdenines(ADN);
        int c = numCitosines(ADN);
        int g = numGuanines(ADN);
        int t = numTimines(ADN);

        if (a < c && a < g && a < t) {
            base = "A";
            min = a;
        } else if (c < a && c < g && c < t) {
            base = "C";
            min = c;
        } else if (g < a && g < c && g < t) {
            base = "G";
            min = g;
        } else {
            base = "T";
            min = t;
        }

        return base;
    }
    
    /**
     * Funció que recompta totes les bases y les suma
     * 
     * @param ADN
     * @return totalBases
     */
    public int totalBases(String ADN){
        int totalBases = 0;
        int a = numAdenines(ADN);
        int c = numCitosines(ADN);
        int g = numGuanines(ADN);
        int t = numTimines(ADN);
        
        totalBases = a+c+g+t;
        
        return totalBases;
    }
    
    /**
     * Funció que calcula el percentatge de la base rebuda per paràmetre
     * 
     * @param ADN
     * @param base
     * @return percentage
     */
    public float calculatePercentage(String ADN, int base){
        int totalBases = totalBases(ADN);
        float percentage = base * 100 / totalBases;
        return percentage;
        
    }
    
    /**
     * Funció que calcula el percentatge d'Adenines a la cadena
     * 
     * @param ADN
     * @return percentageA
     */
    public float percentageAdenines(String ADN){
        
        int numA = numAdenines(ADN);
        float percentageA = calculatePercentage(ADN, numA);

        return percentageA;
        
    }
    
    /**
     * Funció que calcula el percentatge de Citosines a la cadena
     * 
     * @param ADN
     * @return percentageC
     */
    public float percentageCitosines(String ADN){          
        int numC = numCitosines(ADN);
        float percentageC = calculatePercentage(ADN, numC);

        return percentageC;
    }
    
    /**
     * Funció que calcula el percentatge de Guanines a la cadena
     * 
     * @param ADN
     * @return percentageG
     */
    public float percentageGuanines(String ADN){       
        int numG = numGuanines(ADN);
        float percentageG = calculatePercentage(ADN, numG);

        return percentageG;
    }
    
    /**
     * Funció que calcula el percentatge de Timines a la cadena
     * 
     * @param ADN
     * @return percentageTimines
     */
    public float percentageTimines(String ADN){      
        int numT = numTimines(ADN);
        float percentageT = calculatePercentage(ADN, numT);

        return percentageT;
    }
    
}
