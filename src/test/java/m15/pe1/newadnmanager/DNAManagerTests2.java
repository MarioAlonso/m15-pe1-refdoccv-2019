/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m15.pe1.newadnmanager;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author tarda
 */
public class DNAManagerTests2 {

    String dnaSequence;
    ADN_Manager dnaFunct;

    public DNAManagerTests2() {
        dnaSequence = "GATATGC";
        dnaFunct = new ADN_Manager();
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    @Test
    public void testMinLetterDNA_COneTime() {
        dnaSequence = "GATATGC";
        String expected = "C";
        String result = dnaFunct.minLetter(dnaSequence);
        assertEquals(result, expected);
    }

    @Test
    public void testMinLetterDNA_T_Twice() {
        dnaSequence = "CCGATACATGAC";
        String expected = "T";
        String result = dnaFunct.minLetter(dnaSequence);
        assertEquals(result, expected);
    }

    @Test
    public void testNumberAdenines() {
        dnaSequence = "GACTACGACTAGCA";
        int expected = 5;
        int result = dnaFunct.numAdenines(dnaSequence);
        assertEquals(result, expected);
    }
    
     @Test
    public void testInvalidNumberAdenines() {
        dnaSequence = "GACTACGACTAGCA";
        int expected = 8;
        int result = dnaFunct.numAdenines(dnaSequence);
        assertEquals(result, expected);
    }

    @Test
    public void testPercentageAdenines() {
        dnaSequence = "GACTACGACTAGCA";
        float expected = (float) 35.0;
        float result = dnaFunct.percentageAdenines(dnaSequence);       
        assertEquals(result, expected);
    }
    
    @Test
    public void testInvalidPercentageAdenines() {
        dnaSequence = "GACTACGACTAGCA";
        float expected = (float) 0.0;
        float result = dnaFunct.percentageAdenines(dnaSequence);       
        assertEquals(result, expected);
    }

    @Test
    public void testMaxLetter() {
        dnaSequence = "CCCCGATGCATC";
        String expected = "C";
        String result = dnaFunct.maxLetter(dnaSequence);
        assertEquals(result, expected);
    }
    
    @Test
    public void testInvalidMaxLetter() {
        dnaSequence = "CCCCGATGCATC";
        String expected = "A";
        String result = dnaFunct.maxLetter(dnaSequence);
        assertEquals(result, expected);
    }

//    TODO Pending DNA full integrity validation.
//    @Test
//    public void testMinLetterDNAInvalidLetterD() {
//        dnaSequence = "GATA";
//        String expected = "C";
//        String result = dnaFunct.minLetter(dnaSequence);
//        assertEquals(result,expected);
//    }
}
